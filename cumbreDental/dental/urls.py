from django.urls import path, include
from . import views

app_name = "dental"

urlpatterns = [
    path('', views.homepage, name='homepage'),
]